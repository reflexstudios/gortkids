---
title: Contact
contact_info:
  -
    type: set_1
    phone_number: '028 86769808'
    email_address: info@gortkids.com
    address: '94 Church Street, Cookstown, BT80 8HX'
contact_form:
  - enquiry
hide_from_navigation: false
fieldset: contact
template: contact
id: bd9be347-4b72-4461-8373-950b6eb40ae6
---
If you have any enquiries or questions feel free to get in contact!  

You can get in touch via the contact form below or if you prefer you can give us a call. Either way we look forward to hearing from you.