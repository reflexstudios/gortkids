---
title: About
about_image_big: /assets/gort-kids-after-schools-club-home-about-services-facilities-contact-us-halloween-2017.jpg
about_image_small: /assets/guernsey-003_thb_thb.jpg
facilities_title: 'Our Facilities'
facilities_content: 'Gort Kids provides a range of facilities suitable for the children who attend the club.  Our resources have been carefully selected to stimulate all age groups of children and encourage them to play together.  The children make great use of the facilities available to them and get much enjoyment from them.'
facilities:
  -
    type: set_1
    facilities_title: 'Home Corner'
    facilities_text: 'The children enjoy playing in our new home corner.  The area is fitted with child-size kitchen units with a realistic appearance and a dressing up trolley.  The children create different themes for the home corner, such as hair dressers, restaurant and shop.'
    facilities_image: /assets/guernsey-003_thb_thb.jpg
  -
    type: set_1
    facilities_title: 'Outdoor Play Area'
    facilities_text: 'We take every possible opportunity to get some fresh air for both children and staff.  Our outdoor play area consists of a climbing frame with a slide and removable swings, football nets and a picnic table.  The children also like to play with space hoopers, skipping ropes, chalk, basketballs, swing ball and scooters.'
    facilities_image: /assets/outdoor-play-area_thb.jpg
  -
    type: set_1
    facilities_title: Homework
    facilities_text: 'The kids have a chance to do their homework in Gort Kids, with staff available for help when they need it. They work at their own pace using the club''s resources for assistance.'
    facilities_image: /assets/photo-(5)_thb-(1).jpg
testimonial_title: 'What Our Customers Say'
testimonials:
  -
    type: set_1
    testimonial_text: 'My little lady loves the snacks at Gort Kids, pancakes were delicious today.  Thanks to all the staff!'
    testimonial_name: 'Imelda Mc Crory'
  -
    type: set_1
    testimonial_text: 'Gort Kids is a real home from home for my daughter.  So glad that we have such great girls running it and that we have it in Cookstown!'
    testimonial_name: 'Sharon Mc Guckin'
  -
    type: set_1
    testimonial_text: 'I like Gort Kids because we do lots of activities and play in the afternoon. I like to paint and do lots of Art. I like to do my homework in Gort Kids and I like snack.'
    testimonial_name: 'Ella Neeson'
hide_from_navigation: false
about_image:
  - /assets/about_image.png
page_title: About
fieldset: about
template: about
id: f6c6db46-67af-4a71-b81c-1d52e33b5d0b
---
<p><strong><i>Gort Kids is a cross community after schools club catering for the needs of 4-12 year olds and their families in the Cookstown area.</i></strong>
</p>
<p>We welcome all children in this age group into our club and aim to maintain an environment which makes all members feel accepted and supported.
</p>
<p>Here at Gort Kids we offer an out-of-school activity centre for children with the aim to provide high quality, affordable childcare places with a range of stimulating, ineteresting and enjoyable activities. Gort Kids provides supervision and care, permitting parents to enter or continue in education, training or employment.
</p>