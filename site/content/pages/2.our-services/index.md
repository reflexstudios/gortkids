title: Services
services_list:
  -
    type: set_1
    services_title: 'Provision of Snack'
    services_content: 'Snack time is at 15:30 every day, when the kids come together to enjoy some food after a long day at school.  On ocassions when the daily activity is cookery, the kids also get to have some fun preparing their own snacks.  Whilst being enjoyabe, this also provides them experience with core skill sure to be useful after leaving Gort Kids.'
    services_image: /assets/img/july-2009-1322010-01-29-005139_thb.jpg
  -
    type: set_1
    services_title: Activities
    services_content: 'The children take part in a different activity each day.  They work in teams to complete various tasks relating to a specific theme.  The activites range from arts and crafts to cookery and games.  All children are encouraged to take part and all art work and pictures of activities are made into wall displays.  All staff members plan activities as a team, ensuring that they meet the developmental stages of all age groups in the club.'
    services_image: /assets/img/dscf4379_thb.jpg
  -
    type: set_1
    services_title: Homework
    services_content: 'The children are given a choice of doing their homework in the setting or waiting until they get home.  Time is given to complete it after they have done the activity, but only if they wish to do their homework.  The children are encouraged to do their work themselves, andif they need any assistance they can ask the playworkers.'
    services_image: /assets/img/photo-(5)_thb.jpg
  -
    type: set_1
    services_title: 'School Collection'
    services_content: 'The staff at Gort Kids walk to the local schools every day to meet the children and ensure they get to the setting safely.  On the journey from school the children walk with their friends, getting some exercise and fresh air whilst being in safe hands.  The children are fully aware of road safety and each take it in turn to press the button at the pelican crossings.'
    services_image: /assets/img/dscf4391_thb.jpg
  -
    type: set_1
    services_title: 'Summer Trips'
    services_content: 'Each year in our Summer Scheme, the children are taken on various trips.  This Summer, the children have visited the Tayto Factory, Oxford Island, Aunt Sandra\\\\\\\''s Sweet Factory, Waterworld, Dunluce Centre and Belfast Zoo.  Day trips give the children the opportunity to share new experiences with their friends in the club, as well as learning new things, such as how crisps are made at Tayto Castle and tent building, as seen in Oxford Island.'
    services_image: /assets/img/jungle_thb.jpg
hide_from_navigation: false
fieldset: services
template: services
id: c21a3a04-ed9b-4f77-862e-a9787df5d243
