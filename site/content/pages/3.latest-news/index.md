title: News
hide_from_navigation: false
news-list:
  -
    type: set_1
    news-item-image:
      - /assets/homecornerimage.png
    news-item-title: 'Halloween Party'
    news-item-date: '2019-05-15'
  -
    type: set_1
    news-item-title: 'Kid of the Month'
    news-item-date: '2019-05-15'
  -
    type: set_1
    news-item-title: Spring
    news-item-date: '2019-05-15'
  -
    type: set_1
    news-item-title: 'Kid of the Month'
    news-item-date: '2019-05-12'
fieldset: default
template: news/index
mount: news
id: 625446e2-eaa1-45b9-8e40-a8cd7946171f
