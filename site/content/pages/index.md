---
hero-title: 'Gort Kids is a cross community after schools club'
hero-image-top: /assets/kid-of-the-month-september-2017.jpg
hero-image-bottom: /assets/kid-of-the-month-september-2017.jpg
services-title: 'Whats on offer?'
services-content: |
  <p>In our afterschools club, we provide a range of services to ensure parents their children are in the safest possible environment, whilst forming lasting social relationships.With the following provisions in place, it is easier for parents to leave work relaxed with the knowledge that their child(ren) have been in a secure setting, completed their homework, had something to eat and been with friends.
  </p>
news-title: 'Latest News'
title: Home
hide_from_navigation: false
hero-image: /assets/hero-image.png
page_title: 'Gort Kids is a cross community after schools club'
fieldset: home
template: home
id: 59ccb2ed-e29e-4750-afc2-a3437ee91dc9
---
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tincidunt tempor dolor, dapibus accumsan erat venenatis in. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut a sodales nibh. Donec porttitor sem tincidunt volutpat porttitor.
</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam congue dui in iaculis pharetra. Cras tempus purus ut dictum aliquet. Nam nec sem nec ligula eleifend ullamcorper.
</p>
<p>Quisque sagittis sed tellus at suscipit. Morbi vestibulum faucibus convallis. Nam ullamcorper auctor vulputate. Vivamus ultrices ullamcorper porta. Curabitur at malesuada nulla, placerat lobortis odio.
</p>