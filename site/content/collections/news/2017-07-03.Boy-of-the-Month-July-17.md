title: 'Boy of the Month - July 17'
news_image: /assets/boy-of-the-month---july-17.jpg
news_article: |
  <p>As we are busier in the Summer, we have chosen a boy and girl of the month.  The boy of the month has been awarded the title for the second time.  A great role model to the younger children, Davids is kind to others, follows the rules, helps out the staff and participates in and enjoys all group activities.
  </p>
  <p>Congratulations Davids
  </p>
id: ef7e5a08-2a4c-4741-81a5-71372ef7cfe7
