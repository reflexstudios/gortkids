title: 'Kid of the Month - June 2017'
news_image: /assets/kid-of-the-month---june-2017.jpg
news_article: |
  <p>This month's Kid of the Month has been awarded to Rogan Donnelly.
  </p>
  <p>Rogan is a quiet young boy who respects all children and staff in Gort Kids.  He is kind to other and always shares.  Rogan has lots of friends in Gort Kids and enjoys taking part in lots of games and activities with everybody.  He truly deserves the title of Kid of the Month
  </p>
  <p>Well done Rogan
  </p>
id: 1061ddd2-46fb-4383-ba87-5082ca4adc1b
