const NotificationCenter = require('node-notifier').NotificationCenter;
var notifier = new NotificationCenter({
  withFallback: false, // Use Growl Fallback if <= 10.8
  customPath: void 0 // Relative path if you want to use your fork of terminal-notifier
});

const autoprefixer = require('gulp-autoprefixer')
const babel = require('gulp-babel')
const gulp = require('gulp')
const maps = require('gulp-sourcemaps')
const plumber = require('gulp-plumber')
const imagemin = require('gulp-imagemin')
const concat = require('gulp-concat')
const sass = require('gulp-sass')
const uglify = require('gulp-uglify')
const gutil = require('gulp-util')
const browserSync = require('browser-sync')
const connect = require('gulp-connect-php')
const path = require('path')
const pug = require('gulp-pug')

function sitePath() { 
  var tmp = path.resolve(__dirname).split('/')
  var saveFolder = "";
  for (i = 0; i < tmp.length; i++) { 
    if(tmp[i] == 'htdocs') {
      saveFolder = tmp[i+1];
    }
  }
  return saveFolder;
};

const folder = sitePath();

gulp.task('connect', function() {
  connect.server({}, function (){
    browserSync({
      proxy: folder + '.localhost',
      open: false
    });
  });
});

gulp.task('css', () => gulp.src('src/sass/app.scss')
  .pipe(plumber())
  .pipe(maps.init())
  .pipe(sass({
    includePaths: [
      'node_modules/normalize-scss/sass'
    ],
    outputStyle: 'compressed'
  })
  .on('error', function(err) {
    notifier.notify({
      title: 'Gulp Error',
      message: "Sass Error on " + folder,
      sound:    "Funk",
      icon: "http://sass-lang.com/assets/img/styleguide/seal-color-aef0354c.png",
      contentImage: "https://www.reflex-studios.com/imgs/reflex-studios-logo-black.png"
    });
    gutil.log(gutil.colors.magenta(err.messageFormatted));
    this.emit('end');
  }))
  .pipe(autoprefixer({
    browsers: [ 'last 2 versions', 'ie >= 11' ]
  }))
  .pipe(maps.write('./'))
  .pipe(gulp.dest('css'))
)

gulp.task('js', () => gulp.src([
    "src/js/jquery.cycle2.js",
    "src/js/jquery.cycle2.carousel.min.js",
    "src/js/slick.min.js",
    "src/js/rangeslider.min.js",
    "src/js/**/!(scripts)*.js",
    "src/js/**/scripts.js"
  ])
  .pipe(plumber())
  .pipe(babel({
    presets: ['env']
  }))
  .pipe(maps.init())
  .pipe(uglify().on('error', function(err) {
    notifier.notify({
      title: 'Gulp Error',
      message: "JavaScript Error on " + folder,
      sound:    "Funk",
      icon: "http://3.bp.blogspot.com/-PTty3CfTGnA/TpZOEjTQ_WI/AAAAAAAAAeo/KeKt_D5X2xo/s1600/js.jpg",
      contentImage: "https://www.reflex-studios.com/imgs/reflex-studios-logo-black.png"
    });
    gutil.log(gutil.colors.magenta(err));
    this.emit('end');
  }))
  .pipe(plumber.stop())
  .pipe(concat('app.js'))
  .pipe(maps.write('./'))
  .pipe(gulp.dest('js'))
)

gulp.task('img', () => gulp.src('src/img/**')
  .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({plugins: [{removeViewBox: true}]})
  ], {
    verbose: true
  }))
  .pipe(gulp.dest('../../../assets/img'))
)

gulp.task('templates', function() {
  gulp.src('src/views/**/*.pug')
    .pipe(plumber())
    .pipe(pug({
      pretty: false
    }).on('error', function(err) {
      notifier.notify({
        title: 'Gulp Error',
        message: "Pug Error on " + folder,
        sound:    "Funk",
        icon: "https://camo.githubusercontent.com/a43de8ca816e78b1c2666f7696f449b2eeddbeca/68747470733a2f2f63646e2e7261776769742e636f6d2f7075676a732f7075672d6c6f676f2f656563343336636565386664396431373236643738333963626539396431663639343639326330632f5356472f7075672d66696e616c2d6c6f676f2d5f2d636f6c6f75722d3132382e737667",
        contentImage: "https://www.reflex-studios.com/imgs/reflex-studios-logo-black.png"
      });
      gutil.log(gutil.colors.magenta(err));
      this.emit('end');
    }))
    .pipe(gulp.dest(''))
});

gulp.task('build', [ 'css', 'js', 'img' ])

gulp.task('watch', [ 'build' ], () => {
  gulp.watch('src/sass/**/*.scss', [ 'css' ])
  gulp.watch('src/js/**/*.js', [ 'js' ])
  gulp.watch('src/img/**', [ 'img' ])
  gulp.watch([
    'src/views/layouts/**/*',
    'src/views/partials/**/*',
    'src/views/templates/**/*'
  ], ['templates'])
})

gulp.task('default', ['connect', 'watch'])