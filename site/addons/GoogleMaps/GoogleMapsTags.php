<?php

namespace Statamic\Addons\GoogleMaps;

use Statamic\Extend\Tags;
use Illuminate\Support\Facades\Log;

class GoogleMapsTags extends Tags
{
    /**
     * The {{ google_maps }} tag
     *
     * @return string
     */
    public function index()
    {
        $api_key = $this->getConfig('api_key', 'YOUR_API_KEY');
        $zoom = $this->getParamInt('zoom', 10);
        $address = $this->getParam('address', 'Belfast');
        $lng = $this->getParam('lng', '');
        $lat = $this->getParam('lat', '');
        $title = $this->getParam('title', '');

        $map_id = 'gmap' . uniqid();

        if ($api_key == 'YOUR_API_KEY') {
            Log::error("GoogleMapsAddon: No api key specified");
        }

        return $this->view('partials.map', compact('api_key', 'style', 'zoom', 'title', 'address', 'map_id', 'lng', 'lat'));
    }
}
