<div id="{{ $map_id }}" class="map"></div>
<script>
    var map;
    function initMap() {
        var mapLatLng = new google.maps.LatLng({{ $lat }}, {{ $lng }});
		var mapOptions = {
            zoom: {{ $zoom }},
            center: mapLatLng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
		}
        map = new google.maps.Map(document.getElementById('{{ $map_id }}'), mapOptions);

        if(markers.length == 0) {
            var infowindow = new google.maps.InfoWindow({
                content: '<div id="mapContent">' +
                '<p><strong>{{ $title }}</strong></p>' +
                    '<p>{{ $address }}</p>' +
                    '<a class="btn" href="http://maps.google.co.uk/maps?daddr=' + encodeURI('{{ $address }}') + '">Get Directions</a>' +
                '</div>'
            });

            var marker = new google.maps.Marker({
                position: mapLatLng,
                map: map,
                title: '{{ $title }}'
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        } else {
            for(x = 0; x < markers.length; x++)
            {
                var item = markers[x];
                var infowindow = new google.maps.InfoWindow();
                var content = '<div class="mapContent">' +
                    '<p><strong>' + item.title + '</strong></p>' +
                        '<p>' + item.address + '</p>' +
                        '<a class="btn" href="http://maps.google.co.uk/maps?daddr=' + encodeURI(item.address) + '">Get Directions</a>' +
                    '</div>';
                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(item.latitude, item.longitude),
                    map: map,
                    title: item.title,
                    icon: item.icon
                });

                google.maps.event.addListener(marker,'click', (function(marker,content,infowindow) { 
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map,marker);
                    };
                })(marker,content,infowindow)); 
            }   
        }

    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ $api_key }}&callback=initMap" async defer></script>
