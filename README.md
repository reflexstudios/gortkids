# Reflex Studios Statamic Website

To be used for creating a new Reflex Studios Statamic website.

## Requirements

- Composer
- Node and npm
- Laravel Valet

## Set Up

Run the following commands in your terminal to setup a new website

1. git clone git@bitbucket.org:reflexstudios/setup.git YOURSITENAME (keep all lowercase and one word, eg. gortkids or redwidgets)
2. cd YOURSITENAME
3. rm -rf .git
4. composer install -d statamic
5. cd site/themes/reflex
6. npm install
7. Visit YOURSITENAME.localhost on your browser to confirm all is working

## Admin login

You can access the admin with a defaut username and password

- YOURSITENAME.localhost/cp
- Username: hello@reflex-studios.com
- Password: reflexstudios

Change your local version to get your own username and password and delete the default

## Modifying Code

Run these before to allow you to modify code

1. cd site/themes/reflex
2. gulp watch

## Setup Git

If the Site doesn't have a repo, create one in Bitbucket. Ensure your assign it the right client name and repo name.

1. git init
2. git add .
3. git remote add origin git@bitbucket.org:reflexstudios/YOURSITENAME.git
4. git push -u origin master
